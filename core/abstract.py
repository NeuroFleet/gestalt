#-*- coding: utf-8 -*-

from gestalt.core.libs import *

###################################################################################

class Helper(object):
    def __init__(self, ancestor, *args, **kwargs):
        self._prnt = ancestor

        self.trigger('initialize', *args, **kwargs)

    ancestor = property(lambda self: self._prnt)

    def trigger(self, method, *args, **kwargs):
        handler = getattr(self, method, None)

        if callable(handler):
            return handler(*args, **kwargs)
        else:
            return None

###################################################################################

class ReactorExt(Helper):
    reactor = property(lambda self: self.ancestor)

    def initialize(self, *args, **kwargs):
        pass

#***************************************************************************

reactor_factory = {}

def extend_reactor(*args, **kwargs):
    def do_apply(handler, alias):
        if alias not in reactor_factory:
            reactor_factory[alias] = handler

        return handler

    return lambda hnd: do_apply(hnd, *args, **kwargs)

###################################################################################

class Nucleon(Helper):
    def initialize(self, *args, **kwargs):
        self._stt  = None
        self._grp = None

        self._nrw = os.environ.get('GHOST_NARROW', 'uchikoma')
        self._bot = os.environ.get('GHOST_PERSON', 'proto')

        self._dns = os.environ.get('GHOST_DOMAIN', '%s.pw' % self._nrw)

        self._app = os.environ.get('UCHIKOMA_APPs', 'connector console').split(' ')

        while "'" in self._dns:
            self._dns = self._dns.replace("'","")

        from os.path import abspath, dirname

        self._root = os.environ.get('GHOST_FOLDER', abspath(dirname(dirname(dirname(__file__)))))

        sys.path.insert(0, self.bpath)

        self.trigger('loading')

        self._dbg = True

        for alias,handler in [kv for kv in reactor_factory.iteritems()]:
            mgr = handler(self)

            setattr(self, alias, mgr)

        self.trigger('loaded')

    #***************************************************************************

    debug        = property(lambda self: self._dbg)
    bpath        = property(lambda self: self._root)

    environ      = property(lambda self: os.environ)

    domain       = property(lambda self: self._dns)
    namespace    = property(lambda self: self._nrw)
    personna     = property(lambda self: self._bot)

    applications = property(lambda self: self._app)

    #***************************************************************************

    def rpath(self, *path): return os.path.join(self.bpath, *path)

    def FILES(self, *path): return self.rpath('opt','django', *path)
    def VERSE(self, *path): return self.rpath('srv', self.namespace, *path)

    def SPECS(self, *path): return self.VERSE('agents', self.personna, *path)
    def THEMES(self, alias, *path): return self.FILES('themes', alias, *path)
    def CORPORA(self, *path): return self.rpath('usr','corpora', *path)

    #***************************************************************************

    def varenv(self, key, default=None):
        resp = None

        if key in self.environ:
            resp = self.environ[key]

        if resp is None:
            resp = default
        elif len(str(resp).strip())==0:
            resp = default

        if type(resp) in (str,unicode):
            if len(resp.strip())==0:
                resp = default

            for sep in ('"', "'"):
                if resp.startswith(sep) and resp.endswith(sep):
                    resp = resp[1:-1]

        if self.debug:
            pass # print "ENV) %s \t: %s" % (key, resp)

        return resp

    #***************************************************************************

    def varlink(self, key, default=None):
        resp = self.varenv(key, default)

        try:
            return urlparse(resp)
        except Exception,ex1:
            try:
                return urlparse(default)
            except Exception,ex2:
                return None

    #***************************************************************************

    @property
    def settings(self):
        if self._stt is None:
            return default_settings

        return self._stt

    def setup(self, target):
        self._stt = target

        self._grp = None ; x =self.graph

    ############################################################################

    #def get_site_info(self, request):
    def get_site_info(self):
        resp = {
            'sections':  [],
        }

        from uchikoma import settings as stt

        for key,entry in self.router.subdomains.iteritems():
            resp['sections'].append(entry)

        return resp

    #***************************************************************************

    def _status_store(self, alias, link):
        filename = 'django-watchman-{}.txt'.format(uuid.uuid4())
        content = 'django-watchman test file'

        try:
            path = default_storage.save(filename, ContentFile(content))

            default_storage.size(path)
            default_storage.open(path).read()
            default_storage.delete(path)

            return {
                "state": True,
            }
        except Exception,ex:
            return {
                "state": False,
                "error": str(ex),
            }

    #***************************************************************************

    def status_email(self, alias, link):
        headers = {"X-DJANGO-WATCHMAN": True}

        headers.update(watchman_settings.WATCHMAN_EMAIL_HEADERS)

        email = EmailMessage(
            "django-watchman email check",
            "This is an automated test of the email system.",
            "watchman@example.com",
            watchman_settings.WATCHMAN_EMAIL_RECIPIENTS,
            headers=headers,
        )

        email.send()

        return {"state": True}
