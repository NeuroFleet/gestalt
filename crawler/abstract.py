from .helpers import *

################################################################################

class WebFile(object):
    def __init__(self, engine, link, payload):
        self._eng = engine
        self._lnk = link
        self._raw = payload

        self.initialize()

    engine   = property(lambda self: self._eng)
    link     = property(lambda self: self._lnk)
    source   = property(lambda self: self._raw)

    url      = property(lambda self: self.link.url)
    referrer = property(lambda self: self.link.referrer)

    def match(self, pattern):
        return self.link.match(pattern)

    def xpath(self, expression):
        return self.xml_tree.xpath(expression)

    #***************************************************************************

    @classmethod
    def retrieve(cls, eng, link):
        from pattern import web as pWeb

        payload,target = None,URL(link.url)

        try:
            payload = target.download(cached=True)
        except Exception,ex:
            eng.trigger('failed', link, ex)

        eng.trigger('visit', link)

        if payload is not None:
            handler = None

            for wrapper,listing in [
                #(Web_TEXT,  pWeb.MIMETYPE_PLAINTEXT),
                (Web_HTML, pWeb.MIMETYPE_WEBPAGE),
                (Web_PDF,  pWeb.MIMETYPE_PDF),
                #(Web_NEWS, pWeb.MIMETYPE_NEWSFEED)
            ]:
                if target.mimetype in listing:
                    handler = wrapper

            if handler is not None:
                return handler(eng, link, payload)

        return None

################################################################################

class Web_HTML(WebFile):
    def initialize(self):
        self._dom = DOM(self.source)

        self._xml = etree.HTML(self.source)

    dom_tree = property(lambda self: self._dom)
    xml_tree = property(lambda self: self._xml)

    def css(self, expression):
        return self.dom_tree(expression)

#*******************************************************************************

class Web_PDF(WebFile):
    def initialize(self):
        self._pdf = PDF(self.source)

    document = property(lambda self: self._pdf)

    pdf_tree = property(lambda self: self.document.doc)
    xml_tree = property(lambda self: self.document.tree)

    def pq(self, *args, **kwargs):
        return self.document.pq(*args, **kwargs)

    def extract(self, *args, **kwargs):
        return self.document.extract(*args, **kwargs)

