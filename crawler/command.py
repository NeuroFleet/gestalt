#!/usr/bin/python

import os, click, yaml

################################################################################

curr,specs = None,None

from os.path import abspath, dirname, join, exists

CURR,ROOT = None,dirname(dirname(dirname(abspath(__file__))))

bpath = lambda *x: join(ROOT, *x)

################################################################################

@click.group()
#@click.option('--count', default=1, help='number of greetings')
@click.argument('site')
@click.pass_context
def cli(ctx, site):
    ctx.obj['root'] = ROOT

    for app in os.listdir(bpath('uchikoma')):
        pth = bpath('uchikoma', app, 'crawl', 'domains', site)

        if exists(pth):
            ctx.obj['path'] = pth

    rpath = lambda *x: join(ctx.obj['path'], *x)

    specs = yaml.load(open(rpath('manifest.yml')))
    codes = open(rpath('crawler.py')).read()

    exec codes in globals(), locals()

    if len(specs.get('targets', []))==0:
        specs['targets'] = [
            'http://%s/' % fqdn
            for fqdn in specs['domains']
        ]

    ctx.obj['spider'] = Spider(**specs)

################################################################################

@cli.command()
@click.pass_context
def init(ctx):
    click.echo('Initialized the database')

#*******************************************************************************

@cli.command()
@click.pass_context
def spider(ctx):
    click.echo('Running the spider :')

    ctx.obj['spider'].start()

################################################################################

if __name__ == '__main__':
    cli(obj={})

