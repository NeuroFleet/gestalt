from gestalt.core.libs import *

#*******************************************************************************

from django import forms

from django.conf import urls as dj_urls
from django.conf import settings as default_settings

from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.core.mail import EmailMessage

from django.db import models, connections as sql_conns

from django.http import JsonResponse

from django.shortcuts import redirect
from django.utils import timezone

from django.contrib import messages

#*******************************************************************************

from django_mongoengine import Document         as MongoDocument
from django_mongoengine import EmbeddedDocument as MongoEmbedded
from django_mongoengine import fields           as MongoProp

