import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "%(GHOST_NARROW)s.settings" % os.environ)

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()

# from helloworld.wsgi import HelloWorldApplication
# application = HelloWorldApplication(application)

from whitenoise import WhiteNoise

#application = WhiteNoise(application, root='files/static')
#application.add_files('files/media', prefix='media/')
#application.add_files('files/static', prefix='static/')

