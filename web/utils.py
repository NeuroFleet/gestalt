from gestalt.core.utils import *

from gestalt.web.helpers import *

if Reactor:
    pass

#from gestalt.web.helpers import Reactor

################################################################################

from jsonfield import JSONField
from picklefield.fields import PickledObjectField

from uuidfield import UUIDField
from phonenumber_field.modelfields import PhoneNumberField

from netaddr import EUI, mac_bare
from macaddress import format_mac
from macaddress.fields import MACAddressField

from django_countries.fields import CountryField

#from django.contrib.gis.geos import Point
#from location_field.models.spatial import LocationField
#from location_field.models.plain import PlainLocationField

################################################################################

from django_rq import job           as rq_job
from django_rq import get_queue     as rq_queue
from django_rq import get_scheduler as rq_scheduler

#*******************************************************************************

################################################################################

from django.core.urlresolvers import reverse
from django.utils import timezone

from django.db import models

from urlparse import urlparse

#*******************************************************************************

PROTOCOLs = (
    ('postgres', "PostgreSQL database"),
    ('mysql',    "MySQL database"),

    ('mongodb',  "MongoDB server"),
    ('couchdb',  "CouchDB server"),

    ('gremlin',  "Gremlin"),
    ('neo4j',    "Neo4j Cypher"),

    ('hdfs',     "Hadoop F.S"),

    ('elastic',  "Elastic Search"),
)

FORMATs = (
    ('json',     "JSON"),
    ('xml',      "XML"),
    ('yaml',     "YAML"),

    ('json-ld',  "JSON-LD"),
    ('rdf',      "RDF"),
    ('n3',       "N3 / Turtle"),

    ('geojson',  "GeoJSON"),
    ('xml',      "XML"),
    ('yaml',     "YAML"),
)

CORTEXs = (
    ('wernick', "Wernick"),
    ('broca',   "Broca"),

    ('reason',  "Reasoner"),
    ('learn',   "Learner"),
    ('assess',  "Assessement"),
    ('witness', "Witnessing"),
)

SPINEs = (
    ('acoustic', "Acoustic Nerve"),
    ('visual',   "Visual Nerve"),
)

#*******************************************************************************

class DataSet(object):
    def __init__(self, parent, narrow, data):
        self._prn = parent
        self._nrw = narrow
        self._dtn = data

        if hasattr(self, 'initialize'):
            if callable(self.initialize):
                self.initialize()

    parent  = property(lambda self: self._prn)
    narrow  = property(lambda self: self._nrw)
    data    = property(lambda self: self._dtn)

    realm   = property(lambda self: self.parent.realm)
    db      = property(lambda self: self.parent.db)

    tribe   = property(lambda self: self.db[self.COLLECTION])

    def persist(self):
        pass

################################################################################


