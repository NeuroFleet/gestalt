#-*- coding: utf-8 -*-

from gestalt.core.helpers import *

###################################################################################

@extend_reactor('graph')
class Extension(ReactorExt):
    def initialize(self):
        self._nodes = {}
        self._edges = {}

    nodes      = property(lambda self: self._nodes)
    edges      = property(lambda self: self._edges)

    #***************************************************************************

    @property
    def graph(self):
        if self._grp is None:
            if self._stt is not None:
                backend = 'graphene'

                if backend in self.settings.NEO4J_DATABASES:
                    link = self.settings.NEO4J_DATABASES[backend]

                    link['ENDPOINT'] = link['ENDPOINT'] or '/db/data'

                    self._grp = Neo4jServ.Graph(Neo4jServ.Config('http://%(HOST)s:%(PORT)d%(ENDPOINT)s' % link, link['USERNAME'], link['PASSWORD']))

                    for alias,handler in self._nodes.iteritems():
                        self._grp.add_proxy(alias, handler)

                    for alias,handler in self._edges.iteritems():
                        self._grp.add_proxy(alias, handler)

        return self._grp

    #***************************************************************************

    def _status_neo4j(self, alias, link):
        link['ENDPOINT'] = link['ENDPOINT'] or 'db/data'

        url = 'http://%(HOST)s:%(PORT)d/%(ENDPOINT)s' % link

        try:
            cfg = Neo4jServ.Config(url, link['USERNAME'], link['PASSWORD'])
        except:
            cfg = Neo4jServ.Config(url)

        try:
            grp = Neo4jServ.Graph(cfg)

            return {
                "state": True,
                "nodes": len(grp.E),
                "edges": len(grp.V),
            }
        except Exception,ex:
            return {
                "state": False,
                "error": str(ex),
            }

    ############################################################################

    def register_node(self, *args, **kwargs):
        def do_apply(handler, alias, element=None):
            element = element or alias

            setattr(handler, 'element_type', element)

            if alias not in self._nodes:
                self._nodes[alias] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #***************************************************************************

    def register_edge(self, *args, **kwargs):
        def do_apply(handler, alias, label=None):
            label = label or alias

            setattr(handler, 'label', label)

            if alias not in self._edges:
                self._edges[alias] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

