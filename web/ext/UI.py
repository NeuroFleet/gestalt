#-*- coding: utf-8 -*-

from __future__ import unicode_literals

from gestalt.core.helpers import *

from django.template import Template, Context, RequestContext
from django.template.loader import get_template

from django.views.generic.base import View, TemplateView

################################################################################

@extend_reactor('ui')
class Extension(ReactorExt):
    def initialize(self):
        self._blk = {}
        self._wdg = {}
        self._chr = {}

    def loaded(self):
        from django.template.base import add_to_builtins

        #import django.template.loader

        from django.conf import settings

        for tags in settings.TEMPLATE_TAG_BUILTINS.values():
            add_to_builtins(tags)

    blocks      = property(lambda self: self._blk)
    widgets     = property(lambda self: self._wdg)
    charts      = property(lambda self: self._chr)

    ############################################################################

    class Widget(object):
        def __init__(self, provider, alias, **payload):
            defaults = dict(
                title=alias.capitalize(), heading="",
                icon='',
            )

            for k,v in defaults.iteritems():
                if k not in payload:
                    payload[k] = v

            self._hnd = provider
            self._key = alias
            self._hdg = payload['heading']
            self._lbl = payload['title'] or alias.capitalize()
            self._ico = payload['icon']
            self._box = payload.get('size', dict(md=3))

            for k in (['size'] + defaults.keys()):
                if k in payload:
                    del payload[k]

            self._raw = payload

            for dim in ('lg','sm','xs'):
                if dim not in self._box:
                    self._box[dim] = self._box.get('md', 3)

        provider = property(lambda self: self._hnd)
        payload  = property(lambda self: self._raw)
        size     = property(lambda self: self._box)

        alias    = property(lambda self: self._key)
        title    = property(lambda self: self._lbl)
        heading  = property(lambda self: self._hdg)
        icon     = property(lambda self: self._ico)

        template = property(lambda self: 'widgets/%s.html' % self.provider)

        @property
        def context(self):
            return {
                'widget': {
                    'type':  self.provider,
                    'alias': self.alias,
                    'title': self.title,
                    'icon':  self.icon,
                    'size':  self.size,
                },
                'payload': self.payload,
            }

        def render(self):
            tpl = get_template(self.template)

            return tpl.render(Context(self.context))

    #***************************************************************************

    class Block(Widget):
        template = property(lambda self: 'widgets/blocks/%s.html' % self.provider)

    #***************************************************************************

    class Chart(Widget):
        template = property(lambda self: 'widgets/charts/%s.html' % self.provider)

    ################################################################################

    class SpecialView(View):
        def get_context_data(self, request, *args, **kwargs):
            context = {} # super(SpecialView, self).get_context_data(**kwargs)

            context = self.contextualize(request, **context)

            return context

        def get(self, request, *args, **kwargs):
            from django.shortcuts import render_to_response

            context = self.get_context_data(request, **kwargs)

            return render_to_response(self.template_name, RequestContext(request, context))

        def post(self, request, *args, **kwargs):
            from django.shortcuts import render_to_response

            context = self.get_context_data(request, **kwargs)

            return render_to_response(self.template_name, RequestContext(request, context))

    #***************************************************************************

    class Dashboard(SpecialView):
        def contextualize(self, request, **context):
            if hasattr(self, 'enrich'):
                context = self.enrich(request, **context)

            context['dash']  = dict(
                blocks  = [],
                charts  = [],
                widgets = [],
            )

            for widget in self.populate(request, **context):
                label = type(widget).__name__.lower() + 's'

                if label not in context['dash']:
                    label = 'widgets'

                context['dash'][label].append(widget)

            return context

    #***************************************************************************

    class Tool(SpecialView):
        def contextualize(self, request, **context):
            context['form'] = self.form_class()

            if request.method=='POST':
                context['form'] = self.form_class(request.POST)
                
                if context['form'].is_valid():
                    context = self.process(request, context, context['form'].cleaned_data)

            return context

