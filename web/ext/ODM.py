#-*- coding: utf-8 -*-

from gestalt.core.helpers import *

from mongonaut.sites import MongoAdmin

###################################################################################

@extend_reactor('mongo')
class Extension(ReactorExt):
    def initialize(self):
        self._mongo = {
            'full':  {},
            'class': {},
            'alias': {},
        }
        self._embed = {
            'full':  {},
            'class': {},
            'alias': {},
        }

    schemas     = property(lambda self: self._mongo['alias'])
    embedded    = property(lambda self: self._embed['alias'])

    #***************************************************************************

    def _status_mongo(self, alias, link):
        from pymongo import MongoClient

        if 'port' not in link:
            link['port'] = 27017

        cnx = MongoClient(
            host=link['host'],
            port=link['port'],
        )

        try:
            return {
                "state": True,
                "count": len(cnx[link['name']].objects()),
            }
        except Exception,ex:
            return {
                "state": False,
                "error": str(ex),
            }

    ############################################################################

    class SchemaAdmin(MongoAdmin):
        permissions = property(lambda self: getattr(self, 'PERMISSIONs', ['view', 'edit', 'add', 'delete']))

        def has_view_permission(self, request):   return 'view'   in self.permissions
        def has_edit_permission(self, request):   return 'edit'   in self.permissions
        def has_add_permission(self, request):    return 'add'    in self.permissions
        def has_delete_permission(self, request): return 'delete' in self.permissions

    def register_schema(self, *args, **kwargs):
        def do_apply(handler, alias):
            if not hasattr(handler, 'mongoadmin'):
                mgr = MongoAdmin()

                if hasattr(handler, 'Admin'):
                    mgr = handler.Admin()

                handler.mongoadmin = mgr

            app,key = handler.__module__, handler.__class__.__name__,

            if app not in self._mongo['full']:
                self._mongo['full'][app] = {}

            if key not in self._mongo['full'][app]:
                self._mongo['full'][app][key] = handler

            if alias not in self._mongo['alias']:
                self._mongo['alias'][alias] = handler

            if handler not in self._mongo['class']:
                self._mongo['class'][handler] = {}

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #***************************************************************************

    def register_embed(self, *args, **kwargs):
        def do_apply(handler, alias):
            app,key = handler.__module__, handler.__class__.__name__,

            if app not in self._embed['full']:
                self._embed['full'][app] = {}

            if key not in self._embed['full'][app]:
                self._embed['full'][app][key] = handler

            if alias not in self._embed['alias']:
                self._embed['alias'][alias] = handler

            if handler not in self._embed['class']:
                self._embed['class'][handler] = {}

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

