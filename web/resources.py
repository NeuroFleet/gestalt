from gestalt.web.utils import *
from gestalt.core.shortcuts import *

#*******************************************************************************

from tastypie                       import fields as TastyFields
from tastypie                       import utils
from tastypie.api                   import Api as TastyApi, NamespacedApi
from tastypie.authentication        import Authentication, SessionAuthentication
from tastypie.resources             import Resource, ModelResource, NamespacedModelResource
from tastypie_mongoengine.resources import MongoEngineResource

############################################################################

class ResourceMixin:
    def prepend_urls(self):
        """
        Returns a URL scheme based on the default scheme to specify
        the response format as a file extension, e.g. /api/v1/users.json
        """
        return [
            url(r"^(?P<resource_name>%s)\.(?P<format>\w+)$" % self._meta.resource_name, self.wrap_view('dispatch_list'), name="api_dispatch_list"),
            url(r"^(?P<resource_name>%s)/schema\.(?P<format>\w+)$" % self._meta.resource_name, self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/set/(?P<pk_list>\w[\w/;-]*)\.(?P<format>\w+)$" % self._meta.resource_name, self.wrap_view('get_multiple'), name="api_get_multiple"),
            url(r"^(?P<resource_name>%s)/(?P<pk>\w[\w/-]*)\.(?P<format>\w+)$" % self._meta.resource_name, self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def determine_format(self, request):
        """
        Used to determine the desired format from the request.format
        attribute.
        """
        if (hasattr(request, 'format') and
                request.format in self._meta.serializer.formats):
            return self._meta.serializer.get_mime_for_format(request.format)
        return super(Model, self).determine_format(request)

    def wrap_view(self, view):
        @csrf_exempt
        def wrapper(request, *args, **kwargs):
            request.format = kwargs.pop('format', None)
            wrapped_view = super(Model, self).wrap_view(view)
            return wrapped_view(request, *args, **kwargs)
        return wrapper

#***************************************************************************

class TastyAuth:
    class Simple(Authentication):
        pass

    class Session(SessionAuthentication):
        pass

############################################################################

class TastyView(Resource, ResourceMixin):
    pass

class TastyResource(Resource, ResourceMixin):
    pass

#***************************************************************************

class TastyModel(NamespacedModelResource, ResourceMixin):
    pass

class TastySchema(MongoEngineResource, ResourceMixin):
    pass

