#!/usr/bin/python

import os, sys

import yaml, simplejson as json

rpath = lambda *x: os.path.join(os.environ['GHOST_FOLDER'], *x)
shell = lambda *x: os.system(' '.join([y if (' ' not in y) else ('"%s"' % y) for y in x]))

if __name__=='__main__':
    package = json.loads(open(rpath('opt','hubot','metadata.json')).read())
    scripts = {
        'hubot': ['hubot-'+x for x in (
            'help','diagnostics',
            'cron','heroku-keepalive',
            'pubsub','mongodb-brain','partyline',
        )],
    }

    for bpath in ['%(GHOST_SPECS)s/converse' % os.environ]+[
        rpath('lib','python','dist', os.environ['GHOST_NARROW'], app, 'converse')
        for app in os.listdir(rpath('lib','python','dist', os.environ['GHOST_NARROW']))
    ]:
        if os.path.exists(rpath(bpath, 'manifest.yml')):
            cfg = yaml.load(open(rpath(bpath, 'manifest.yml')))

            if cfg is not None:
                for key,lst in cfg.iteritems():
                    if key not in scripts:
                        scripts[key] = []

                    for item in lst:
                        scripts[key].append(item)

        if os.path.exists(rpath(bpath, 'packages.json')):
            lst, pth = {}, rpath(bpath, 'packages.json')

            try:
                lst = json.loads(open(pth).read())
            except Exception,ex:
                print pth, ex

            for name,version in lst.iteritems():
                package['dependencies'][name] = version

        if os.path.exists(rpath(bpath, 'scripts')):
            for src in os.listdir(rpath(bpath, 'scripts')):
                shell('cp', '-afR', rpath(bpath, 'scripts', src), 'scripts/%s' % src)

    f = open('package.json', 'w+')
    f.write(json.dumps(package, sort_keys=True, indent=4, separators=(',', ': ')))
    f.close()

    for key,lst in scripts.iteritems():
        f = open('%s-scripts.json' % key, 'w+')
        f.write(json.dumps(lst, sort_keys=True, indent=4, separators=(',', ': ')))
        f.close()

    #os.system('npm install')

