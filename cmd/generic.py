#!/usr/bin/python

import os, sys

from gestalt.shell.shortcuts import *

########################################################################################

CONFIG = {}

reactor = Nucleon.instance(
    os.environ.get('GHOST_MODULE', 'django'),
    os.environ.get('GHOST_NARROW', 'default'),
**CONFIG)

#***************************************************************************************

upath = lambda *x: os.path.join(os.environ['UCHIKOMA_ROOT'], *x)
pyenv = lambda *x: upath('var', 'pyenv', *x)

#***************************************************************************************

class Ghost(object):
    @classmethod
    def populate(cls):
        return os.listdir(os.path.join(os.environ.get('UCHIKOMA_ROOT', '/app'), 'home'))

    @classmethod
    def from_env(cls, root=None, person=None):
        if root is None:
            root = os.environ.get('UCHIKOMA_ROOT', '/app')

        if person is None:
            lst = cls.populate()

            if 'GHOST_PERSON' in os.environ:
                person = os.environ['GHOST_PERSON']
            elif len(lst):
                person = lst[0]

        return cls(root, person, os.environ.get('GHOST_MODULE', 'console'))

    def __init__(self, root, ghost, verse):
        self._root  = root
        self._ghost = ghost
        self._verse = verse

        for key,path in {
            'creds': ['credentials.yml'],
            'specs': ['manifest.yml'],
            'spine': ['implant.yml'],
        }.iteritems():
            setattr(self, key, yaml.load(open(self.rpath(*path))))

    root   = property(lambda self: self._root)
    domain = property(lambda self: os.environ.get('GHOST_DOMAIN', 'example.tld'))
    alias  = property(lambda self: self._ghost)
    module = property(lambda self: self._verse)

    bpath  = lambda self, *x: os.path.join(self._root, *x)
    rpath  = lambda self, *x: self.bpath('home', self.alias, *x)

    netloc = property(lambda self: '%s@%s' % (self.alias, self.domain))

    @property
    def context(self):
        resp = dict([
            (field, getattr(self, field))
            for field in ('root','alias','module','netloc','domain')
            if hasattr(self, field)
        ])

        return resp

    def __getitem__(self, key, default=None):
        for xyz in ('context','creds','specs','spine'):
            cnt = getattr(self, xyz)

            if key in cnt:
                return cnt[key]

        return default

    def __setitem__(self, key, value):
        cnt[key] = value

        return value

#***************************************************************************************

@click.group(context_settings=CONTEXT_SETTINGS)
@click.option('--debug/--no-debug', default=False)
@click.option('-v', '--verbose', count=True)
@click.option('--platform', default='local', help='Platform : local , heroku.')
@click.option('--verse', default='console')
@click.pass_context
def cli(ctx, debug, *args, **kwargs):
    if os.environ['PWD']=='/app':
        ctx.obj['platform'] = 'heroku'
    else:
        ctx.obj['platform'] = 'local'

        if os.path.exists(pyenv()):
            #click.echo("VirtEnv : %s" % pyenv())

            ctx.obj['python'] = pyenv('bin','python')

            os.environ['PATH'] += ':'+pyenv('bin')

    ctx.obj['debug'] = ctx.obj['platform'] in ['local']
    ctx.obj.update(kwargs)

    #click.echo("Debug mode is %s" % ('on' if debug else 'off'))

    if 'GHOST_PERSON' in os.environ:
        ctx.obj['ghost'] = Ghost.from_env()
        
        ctx.obj['personna'] = ctx.obj['ghost'].specs
    else:
        ctx.obj['personna'] = None

    reactor.prepare(ctx)

    if os.path.exists(reactor.rpath()):
        os.chdir(reactor.rpath())
    else:
        print "Warning : Application's module '%s' not setup !" % os.environ['GHOST_MODULE']

########################################################################################

def process(soul, entry):
    entry['link'] = entry['link'] % soul
    entry['fqdn'] = '.'.join([entry['name'], soul.alias, soul.domain])

    return entry
